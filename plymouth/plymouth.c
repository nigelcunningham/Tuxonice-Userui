/*
 * userui_plymouth.c - plymouth userspace user interface module.
 *
 * Copyright (C) 2005-2007, Bernard Blackham <bernard@blackham.com.au>
 * Copyright (C) 2019, Nigel Cunningham <nigel@nigelcunningham.com.au>
 * 
 * This file is subject to the terms and conditions of the GNU General Public
 * License v2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 */

#include <linux/vt.h>
#include <sys/resource.h>
#include <sys/ioctl.h>
#include <ctype.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <ply-boot-client.h>

#include "../userui.h"

#define MIN_ADVANCE_PERCENT 5

static int plymouth_ready = 0;
static int userui_plymouth_xres = 0;
static int userui_plymouth_yres = 0;
static int userui_plymouth_verbose = 1;

static void userui_plymouth_prepare() {
}

static void userui_plymouth_cleanup() {
}

static void userui_plymouth_message(__uint32_t section, __uint32_t level,
		__uint32_t normally_logged, char *msg) {
}

static void userui_plymouth_update_progress(__uint32_t value, __uint32_t maximum,
		char *msg) {
    static __uint32_t prev_maximum = -1;
    static __uint32_t old_percent = -1;

    __uint32_t percent;

    if (!plymouth_ready)
	return;

    if (maximum == (__uint32_t)(-1))
	value = maximum = 1;

    if (value > maximum)
	value = maximum;

    if (maximum > 0)
	percent = value * 100 / maximum;
    else
	percent = 100;

    if (prev_maximum == -1)
	prev_maximum = maximum;
    else {
	__uint32_t adv = percent - old_percent;
	if (prev_maximum == maximum && percent != 100 &&
	        0 < adv && adv < MIN_ADVANCE_PERCENT)
	    return;
    }

#if 0
    if (resuming)
       draw_progressbar(percent);
    else
       draw_progressbar(-percent);
#endif
    old_percent = percent;
}

static void userui_plymouth_log_level_change(int loglevel) {
    if (!plymouth_ready)
	return;
}

static void userui_plymouth_redraw() {
    if (!plymouth_ready)
	return;
    // clear_screen();
}

static void userui_plymouth_keypress(int key) {
    if (common_keypress_handler(key))
	return;
    switch (key) {
    }
}

static int plymouth_option_handler(char c)
{
    switch (c) {
	case 'x':
	    //sscanf(optarg, "%d", &userui_plymouth_xres);
	    break;
	case 'y':
	    //sscanf(optarg, "%d", &userui_plymouth_yres);
	    break;
	case 'q':
	    userui_plymouth_verbose = 0;
	    break;
	default:
	    return 0;
    }
    return 1;
}

static char *plymouth_cmdline_options()
{
    return 
"\n"
"  USPLASH:\n"
"  -q, --quiet\n"
"     Enables plymouth's quiet mode (no text is shown).\n";
}

static struct option userui_plymouth_longopts[] = {
    {"quiet", 0, 0, 'q'},
    {NULL, 0, 0, 0},
};

struct userui_ops userui_plymouth_ops = {
    .name = "plymouth",
    //.load = userui_plymouth_load,
    .prepare = userui_plymouth_prepare,
    .cleanup = userui_plymouth_cleanup,
    .message = userui_plymouth_message,
    .update_progress = userui_plymouth_update_progress,
    .log_level_change = userui_plymouth_log_level_change,
    .redraw = userui_plymouth_redraw,
    .keypress = userui_plymouth_keypress,

    /* cmdline options */
    .optstring = "x:y:q",
    .longopts  = userui_plymouth_longopts,
    .option_handler = plymouth_option_handler,
    .cmdline_options = plymouth_cmdline_options,
};
